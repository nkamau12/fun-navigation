import java.util.ArrayList;

public class Path {
	
	
	
	long id;
	ArrayList<Way> ways;
	
	public Path(Long id) {
		this.id = id;
		ways = new ArrayList<Way>();
	}
	
	
	
	/**
	 * Adds a Way to the current path.
	 * 
	 * @param w			New Way being added.
	 * @return boolean	True if Way was added, false otherwise.
	 */
	public boolean addWay(Way w) {
		if (ways.size() == 0) {
			ways.add(w);
			return true;
		} else {
			if (matchingEnds(w)) {
				ways.add(w);
				return true;
			} else {
				return false;
			}
		}
	}
	
	
	
//	Removes the last way in this Path.
	public boolean removeLast() {
		if (ways.size() > 0) {
			ways.remove(ways.size()-1);
			return true;
		} else {
			return false;
		}
	}
	
	
	
	/**
	 * Checks to see if the first Node of a given Way is the same as the
	 * last Node in the current Path.
	 * 
	 * @param w			New Way potentially being added.
	 * @return boolean	True if Way start matches Path end, false otherwise.
	 */
	public boolean matchingEnds(Way w) {
		
		if (w.getSize() < 2) {
			
//			If Way is empty, or only has one Node, returns false, so it can't be added.
			return false;
			
		} else {
			
//			Create a few variables to make the return method look cleaner.
			int lastWayIndex = (ways.size()-1);
			int lastNodeIndex = ways.get(lastWayIndex).getSize()-1;
			Node lastNode = ways.get(lastWayIndex).getNode(lastNodeIndex);
			
//			Returns true if ID of first Node in w matches that of the last Node in current Path.
			return (lastNode.getID() == w.getWayNodes().get(0).getID());
		}
	}
	
	
	
//	Returns the ID of this Path.
	public long getID() {
		return id;
	}
	
	
	
//	Returns an ordered list of the Ways in this Path
	public ArrayList<Way> getWays() {
		return ways;
	}
	
	
	
//	Returns an ordered list of the Nodes in this Path.
	public ArrayList<Node> getNodes() {
		
		ArrayList<Node> nodes = new ArrayList<Node>();
		
		for (Way w : ways) {
			for (Node n : w.getWayNodes()) {
				nodes.add(n);
			}
		}
		
		return nodes;
	}
	
	
	
//	Returns the driving length of the path.
	public double getLengthEst() {
		double length = 0.0;
		for (Way w : ways) {
			length += w.getLengthEst();
		}
		return length;
	}
	
	
	
//	Returns average fun rank of path (using ratio as rank).
	public double getFunRank() {
		if (ways.size() > 0) {
			double rank = 0.0;
			for (Way w : ways) {
				rank += w.getFunRank();
			}
			return rank/ways.size();
		} else {
			return 0;
		}
	}
	public Path clone(){
		Path t= new Path(id);
		for (int i =0;i<ways.size(); i++){
			t.addWay(ways.get(i).clone());
		}
		return t;
	}
}
