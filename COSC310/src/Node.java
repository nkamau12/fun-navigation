
public class Node {

	long id;
	double lat;
	double lon;
	boolean intersection;
	
	/**
	 * Creates a new Node.
	 * Defaults to creating Nodes which are not intersections.
	 * 
	 * @param id	The ID of this Node.
	 * @param lat	The latitude of this Node.
	 * @param lon	The longitude of this Node.
	 */
	public Node(long id, double lat, double lon) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		intersection = false;
	}
	public Node(long id, double lat, double lon,boolean inter) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		intersection = inter;
	}
	
//	Sets this Node as an intersection.
	public void makeIntersection() {
		intersection = true;
	}
	
//	Sets the node as not being an intersection... just in case we need it.
	public void removeIntersection() {
		intersection = false;
	}
	
//	Returns the ID of this Node.
	public long getID() {
		return id;
	}
	
//	Returns the latitude of this Node.
	public double getLat() {
		return lat;
	}
	
//	Returns the longitude of this Node.
	public double getLon() {
		return lon;
	}
	
//	Returns whether or not this Node is an intersection.
	public boolean isIntersection() {
		return intersection;
	}
	public Node clone() {
		return new Node(id, lat, lon, intersection);
	}
	
//	Returns a string of this Node's info.
	public String toString() {
		return new String(getID() + ": " + getLat() + ", " + getLon());
	}
}