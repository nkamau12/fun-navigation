import java.sql.SQLException;
import java.util.ArrayList;


public class FunNav {
	private ArrayList<Path> paths; 
	private Path path1;
	private Connect con;
	private double MAX;
	
	public FunNav(double startlat, double startlon, double max) throws SQLException{
		paths = new ArrayList<Path>();
		path1=new Path((long) 0);
		con = new Connect();
		MAX=max;
		Find(con.FindNode(startlon, startlat));
		System.out.println("search done");
	}
	
	public boolean Find(Way sofar) throws SQLException{
		//check length see if max of path1
		//System.out.println("checking if at target length");
		if(path1.getLengthEst()>MAX){
			save();
			return false;
		}else{
			ArrayList<Way> next =con.FindIntersection(sofar);
			for (int i=0; i<next.size()&&i<3;i++){
				path1.addWay(next.get(i));
				//for(int j=0;j<path1.getNodes().size();j++)
					//System.out.print(path1.getNodes().get(j).id+" ");
				Find(next.get(i));
				//System.out.println("Rolling back");
				path1.removeLast();
			}	
		}
		//System.out.println("loop done");
		if(path1.ways.size()>0){
			return false;
		}else 
			return true;
	}
	
	public void save(){
		if(paths.size()<3){
			paths.add(path1.clone());
		}else{
			if(paths.get(0).getFunRank()<path1.getFunRank()){
				paths.remove(0);
				paths.add(path1.clone());
			}else if(paths.get(1).getFunRank()<path1.getFunRank()){
				paths.remove(1);
				paths.add(path1.clone());
			}else if(paths.get(2).getFunRank()<path1.getFunRank()){
				paths.remove(2);
				paths.add(path1.clone());
			}
		}
	}
	public int nodeCount() {
		int count=0;
		if (paths.size()==0)
			return 0;
		for(int i=0;i<paths.get(0).getNodes().size();i++)
			count++;
		return count;
			
	}
	public Node getNode(int i) {
		return paths.get(0).getNodes().get(i);
	}
}
