import java.util.ArrayList;
import java.lang.Math;

public class Way {
	
	long id;
	ArrayList<Node> nodes;
	
	/**
	 * Creates a new Way.
	 * Ways are initially created empty, with Nodes added later.
	 * 
	 * @param id	The ID of the Way being created.
	 */
	public Way(long id) {
		this.id = id;
		nodes = new ArrayList<>();
	}
	
//	Adds a Node to the Way.
	/**
	 * Adds a Node to this Way.
	 * 
	 * @param n		The Node being added.
	 */
	public void addNode(Node n) {
		nodes.add(n);
	}
	
//	Returns an array of latitude and longitude points for mapping.
	public double [][] getWayPoints() {
		
//		Sets x to be the number of Nodes in this Way.
		int x = nodes.size();
		
//		Creates a 2D array of with 2 columns and as many rows as there are Nodes in this Way.
		double [][] wayPoints = new double [x][2];
		
//		Fills out the array with latitudes in column 0 and longitudes in column 1.
		for (int i = 0; i < x; i++) {
			wayPoints[i][0] = nodes.get(i).getLat();
			wayPoints[i][1] = nodes.get(i).getLon();
		}
		
//		Returns the array of latitude and longitude points in this way.
		return wayPoints;
	}
	
//	Returns the ArrayList of Nodes that make up this Way.
	public ArrayList<Node> getWayNodes() {
		return nodes;
	}
	
//	Returns the node at index i.
	public Node getNode(int i) {
		return nodes.get(i);
	}
	
//	Returns the node at index i.
	public Node getLastNode() {
		return nodes.get(getSize()-1);
	}
	
//	Returns the number of Nodes in this Way.
	public int getSize() {
		return nodes.size();
	}
	
//	Returns the ID of this Way.
	public long getID() {
		return id;
	}
	
//	Returns a string listing the Node ID's in this Way.
	public String toString() {
		
		String thisWay;
		
		if (nodes.size() == 0) {
//			If way is empty, say so.
			thisWay = "This Way is empty.";
		} else if (nodes.size() == 1) {
//			If Way has only one Node, only list the one.
			thisWay = "" + nodes.get(0).getID();
		} else {
//			List all the nodes in this Way.
			thisWay = "Included Nodes: ";
			for (int i = 0; i < (nodes.size()-1); i++) {
				thisWay += nodes.get(i).getID() + ", ";
			}
			thisWay += " " + nodes.get(nodes.size()-1).getID();
		}
		
		return thisWay;
	}
	
//	Returns the straight/arrow/flying distance between the first and last Node in this Way.
	public double straightLength() {
		double length = 2.0;
		if (nodes.size() > 1) {
			double latDist = nodes.get(nodes.size()-1).getLat() - nodes.get(0).getLat();
			double lonDist = nodes.get(nodes.size()-1).getLon() - nodes.get(0).getLon();
			length = Math.hypot(latDist, lonDist);
		}
		return length;
	}
	
//	Returns the distance one would need to drive to get from the first to the last Node in this Way.
	public double drivingLength() {
		
		double length = 0.0;
		
		if (nodes.size() > 1) {
			for (int i = 1; i < nodes.size(); i++) {
				double latDist = nodes.get(i).getLat() - nodes.get(i-1).getLat();
				double lonDist = nodes.get(i).getLon() - nodes.get(i-1).getLon();
				length += Math.hypot(latDist, lonDist);
			}
		}
		return length;
	}
	
//	Returns a double that is the "ratio" of this Way's driving length vs. straight distance.
	public double getFunRank() {
		return drivingLength() / straightLength();
	}
	
//	Returns an estimate of this Way's length in kilometers.
	public double getLengthEst() {
		double tConvert = 1/0.014013882;
		double tLength = drivingLength();
		//tLength * tConvert;
		return 2;
	}
	public Way clone(){
		Way t= new Way(id);
		for (int i =0;i<nodes.size(); i++){
			t.addNode(nodes.get(i).clone());
		}
		return t;
	}
}