package mongo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MapReduceOutput;
import com.mongodb.MongoClient;

/**
 * Program to perform Map-Reduce queries on MongoDB.
 */
public class QueryMongoMapReduce
{
	// TODO: Change this to be your login (first letter of first name + 7 letters of last name)
	/**
	 * MongoDB database name
	 */
	public static final String DATABASE_NAME = "rlawrenc";
	
	/**
	 * MongoDB collection name
	 */
	public static final String COLLECTION_NAME = "data";
	
	/**
	 * Input file for data records
	 */
	private static final String INPUT_FILE = "data/input.txt";
			
	/**
	 * MongoDB Server URL
	 */
	private static final String SERVER = "gpu1.ddl.ok.ubc.ca";
	
	/**
	 * Mongo client connection to server
	 */
	private MongoClient mongoClient;
	
	/**
	 * Mongo database
	 */
	private DB db;
	
	
	
	/**
	 * Main method
	 * 
	 * @param args
	 * 			no arguments required
	 */	
    public static void main(String [] args)
	{
    	QueryMongoMapReduce qmongo = new QueryMongoMapReduce();
    	qmongo.connect();
    	qmongo.load();
    	System.out.println(QueryMongoMapReduce.toString(qmongo.query()));
    	System.out.println(QueryMongoMapReduce.toString(qmongo.query_count()));
    	System.out.println(QueryMongoMapReduce.toString(qmongo.query_count_array_elements()));  
    	System.out.println("\nOutput query #1:");
    	System.out.println(QueryMongoMapReduce.toString(qmongo.query1()));
    	System.out.println("\nOutput query #2:");
    	System.out.println(QueryMongoMapReduce.toString(qmongo.query2()));
    	System.out.println("\nOutput query #3:");
    	System.out.println(QueryMongoMapReduce.toString(qmongo.query3()));
    	System.out.println("\nOutput query #4:");
    	System.out.println(QueryMongoMapReduce.toString(qmongo.query4()));
    	System.out.println("\nOutput query #5:");
    	System.out.println(QueryMongoMapReduce.toString(qmongo.query5()));
    	System.out.println("\nOutput query #6:");
    	System.out.println(QueryMongoMapReduce.toString(qmongo.query6()));
	}
        
    /**
     * Connects to Mongo database and returns database object to manipulate for connection.
     *     
     * @return
     * 		Mongo database
     */
    public DB connect()
    {
    	try
		{
		    // Provide connection information to MongoDB server 
		    mongoClient = new MongoClient(SERVER);
		}
	    catch (Exception ex)
		{	System.out.println("Exception: " + ex);
			ex.printStackTrace();
		}	
		
        // Provide database information to connect to		 
	    // Note: If the database does not already exist, it will be created automatically.
		db = mongoClient.getDB(DATABASE_NAME); 		
		return db;
    }
    
    /**
     * Loads some sample data into MongoDB.
     */
    public void load()
    {					
		DBCollection col;
		// Drop an existing collection (done to make sure we have an empty, new collection each time)
		col = db.getCollection(COLLECTION_NAME);
		if (col != null)
			col.drop();
		
		// Create a collection				
		col = db.createCollection(COLLECTION_NAME, null);
		
		// Load data from JSON documents in a file
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));
			String st;
			
			while ( (st = br.readLine()) != null)
			{
				Object o = com.mongodb.util.JSON.parse(st);
				DBObject dbObj = (DBObject) o;
				col.insert(dbObj);
			}
			br.close();
		}
		catch (IOException e)
		{
			System.out.println("Error loading data from input file: "+INPUT_FILE);
		}													  
	}
      
    /**
     * Performs a MongoDB query that prints out all data.
     */
	public DBCursor query() 
	{		
		DBCollection col = db.getCollection(COLLECTION_NAME);
		BasicDBObject fields = new BasicDBObject();		
		fields.put("_id", 0);
		
		DBCursor cursor = col.find(new BasicDBObject(), fields);
		return cursor;				
	}
	
    /**
     * Performs a MongoDB Map-Reduce query that counts all the documents.  
     * This will return the count of the number of customers for this particular data set.
     */
	public MapReduceOutput query_count() 
	{				
		// JavaScript functions for map and reduce
		// First value ("TotalCustomers") is key, second value (1) is a value associated with that key.  
		String mapfn = "function() { "
						+"emit(\"TotalCustomers\", 1); }";
		String reducefn = "function(key, items) { "
							+"return items.length; }";
		
		System.out.println("\nNumber of documents (customers):");
		DBCollection col = db.getCollection(COLLECTION_NAME);
		MapReduceOutput output = col.mapReduce(mapfn, reducefn, "output_query_count", null);
		
		// Note: This is much easier to do without using Map-Reduce by doing:
		// col.count();
		return output;		
	}
    
	/**
     * Performs a MongoDB Map-Reduce query that counts all the array elements.
     * This will return the total number of orders for all customers for this particular data set.
     */
	public MapReduceOutput query_count_array_elements() 
	{							
		// JavaScript functions for map and reduce
		String mapfn = "function() { "
					+"emit(\"TotalOrders\", this.orders.length); }";
		String reducefn = "function(key, items) { "
						+" return Array.sum(items); }";						
		
		System.out.println("\nNumber of array elements (# of orders):");
		DBCollection col = db.getCollection(COLLECTION_NAME);
		MapReduceOutput output = col.mapReduce(mapfn, reducefn, "output_query_count_array", null);
		
		// Note: Using the MongoDB aggregation framework is easier than using Map-Reduce
		return output;		
	}
	
    /**
     * Performs a MongoDB Map-Reduce query that returns the total number of customers in each state.  SELECT state, COUNT(*) GROUP BY state
     */
    public MapReduceOutput query1()
    {
    	// TODO: Write a MongoDB Map-Reduce query that returns the total number of customers in each state.  SELECT state, COUNT(*) GROUP BY state
    	// See: http://docs.mongodb.org/manual/core/map-reduce/
    	// See: http://docs.mongodb.org/manual/tutorial/map-reduce-examples/
    	// See: http://api.mongodb.org/java/current/com/mongodb/DBCollection.html#mapReduce-java.lang.String-java.lang.String-java.lang.String-com.mongodb.DBObject-
    	    	
    	String mapfn = "function() { "
				+"emit(this.state, 1); }";
    	String reducefn = "function(key, items) { "
					+" return items.length; }";						
	
    	System.out.println("\nNumber of array elements (# of customers):");
    	DBCollection col = db.getCollection(COLLECTION_NAME);
    	MapReduceOutput output = col.mapReduce(mapfn, reducefn, "output_state_query", null);
	
    	// Note: Using the MongoDB aggregation framework is easier than using Map-Reduce
    	return output;
    }
       
   
    /**
     * Performs a MongoDB Map-Reduce query that returns the total value of all orders.  i.e. SUM(orders.total).
     * Output key must be called: "totalOfAllOrders".
     */
    public MapReduceOutput query2()
    {
    	// TODO: Write a MongoDB Map-Reduce query that returns the total value of all orders.  i.e. SUM(orders.total)    	
    	// Note: Output key must be called: "totalOfAllOrders".
    	String mapfn = "function() { "
    			+ "var x=0;"
    			+ "for(var idx=0;idx<this.orders.length;idx++)"
    			+ "x=x+this.orders[idx].total;"
    			+ "emit(\"totalOfAllOrders\", x); }";
    	String reducefn = "function(key, items) { "
					+" return Array.sum(items); }";						
	
    	System.out.println("\nNumber of array elements (# of total Orders):");
    	DBCollection col = db.getCollection(COLLECTION_NAME);
    	MapReduceOutput output = col.mapReduce(mapfn, reducefn, "totalOfAllOrders", null);
    	
    	return output;
    }
    
    /**
     * Performs a MongoDB Map-Reduce query that returns the total value of all orders per state.  SELECT state, SUM(orders.total) GROUP BY state
     */
    public MapReduceOutput query3()
    {
    	// TODO: Write a MongoDB Map-Reduce query that returns the total value of all orders per state.  SELECT state, SUM(orders.total) GROUP BY state 	
    	String mapfn = "function() { "
    			+ "var x=0;"
    			+ "for(var idx=0;idx<this.orders.length;idx++)"
    			+ "x=x+this.orders[idx].total;"
    			+ "emit(this.state, x); }";
    	String reducefn = "function(key, items) { "
					+" return items; }";						
	
    	System.out.println("\nNumber of array elements (# of total Orders):");
    	DBCollection col = db.getCollection(COLLECTION_NAME);
    	MapReduceOutput output = col.mapReduce(mapfn, reducefn, "totalOfAllOrders", null);
    	
    	return output;
    }
    
    /**
     * Performs a MongoDB Map-Reduce query that returns the average # of items per order by state with name > 'K'.  SELECT state, COUNT(orders.items)/COUNT(orders) WHERE name > 'K' GROUP BY state
     */
    public MapReduceOutput query4()
    {
    	// TODO: Write a MongoDB Map-Reduce query that returns the average # of items per order by state with name > 'K'.  SELECT state, COUNT(orders.items)/COUNT(orders) WHERE name > 'K' GROUP BY state    	
    	// Note: For this Map-Reduce you will need to generate a MapReduceCommand object with a filter query and a finalize function (use setFinalize()) then execute use col.mapReduce(cmd).
    	
    	return null;
    }
    
    /**
     * Performs a MongoDB Map-Reduce query that find the order with the maximum # of items. SELECT MAX(orders.item) 
     */
    public MapReduceOutput query5()
    {
    	// TODO: Write a MongoDB Map-Reduce query that find the order with the maximum # of items. SELECT MAX(orders.item) 
    	// Note: Output key should be "max".
    	
    	return null;
    }
    
    /**
     * Performs a MongoDB Map-Reduce query that determines the number of times first name is: longer than last name, same as last name, and shorter than last name.
     */
    public MapReduceOutput query6()
    {
    	// TODO: Write a MongoDB Map-Reduce query that determines the number of times first name is: longer than last name, same as last name, and shorter than last name.
    	// Note: Output keys should be "firstlonger", "firstshorter", and "same".
    	
    	return null;
    }
    
    /**
     * Returns the Mongo database being used.
     * 
     * @return
     * 		Mongo database
     */
    public DB getDb()
    {
    	return db;    
    }
    
    /**
     * Outputs a MapReduceOutput in string form.
     * 
     * @param cursor
     * 		Mongo cursor
     * @return
     * 		results as a string
     */
    public static String toString(MapReduceOutput output)
    {
    	StringBuilder buf = new StringBuilder();
    	int count = 0;
    	buf.append("Rows:\n");
    	if (output != null)
    	{
	    	for (DBObject o : output.results())
			{
				buf.append(o.toString());
				buf.append("\n");
				count++;
			}
    	}
		buf.append("Number of rows: "+count);
		return buf.toString();
    }
    
    /**
     * Outputs a cursor of MongoDB results in string form.
     * 
     * @param cursor
     * 		Mongo cursor
     * @return
     * 		results as a string
     */
    public static String toString(DBCursor cursor)
    {
    	StringBuilder buf = new StringBuilder();
    	int count = 0;
    	buf.append("Rows:\n");
    	Iterator<DBObject> it = cursor.iterator();
		while (it.hasNext()) {
			DBObject obj = it.next();
			buf.append(obj.toString());
			buf.append("\n");
			count++;
		}
		buf.append("Number of rows: "+count);
		return buf.toString();
    }
} 
